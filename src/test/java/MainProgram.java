import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class MainProgram  {
	
	static  WebDriver driver;
	
	public MainProgram() 
	{
		System.setProperty("webdriver.chrome.driver","C:\\\\gecko\\\\chromedriver.exe");
		//System.setProperty("webdriver.gecko.driver","C:\\gecko\\chromedriver.exe");
		driver = new ChromeDriver();
		
	}
	

	public static void main(String[] args) {

		MainProgram test = new MainProgram();
		Login.login(driver);
		CompanyUser.createuser(driver);

	}
	

}
