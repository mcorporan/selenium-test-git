import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GlobalCreditLimit {

	public static void createlimit(WebDriver driver) 
	{
		driver.findElement(By.id("2")).click();
		driver.findElement(By.xpath("//*[@id=\"/company/globalCreditLimit/view/\"]/a")).click();
		driver.findElement(By.id("j_idt105")).click();
		driver.findElement(By.id("name")).sendKeys("Limite Credito Test");
		driver.findElement(By.id("description")).sendKeys("Limite Credito Test");
		driver.findElement(By.id("globalCreditLimitAmount_input")).clear();
		driver.findElement(By.id("globalCreditLimitAmount_input")).sendKeys("3500");
		driver.findElement(By.id("j_idt108")).click();
		WebDriverWait wait1 = new WebDriverWait(driver, 10);
		wait1.until((ExpectedConditions.visibilityOfElementLocated(By.id("immovableAmount_input"))));
		driver.findElement(By.id("immovableAmount_input")).click();
		driver.findElement(By.id("immovableAmount_input")).clear();
		driver.findElement(By.id("immovableAmount_input")).sendKeys("3500");
		driver.findElement(By.xpath("//*[@id=\"serviceCreditLimit:1\"]")).click();
		driver.findElement(By.xpath("//*[@id=\"j_idt199\"]/span[2]")).click();
		driver.findElement(By.xpath("//*[@id=\"j_idt156\"]")).click();
				
	}
	
	public static void deletelimit(WebDriver driver) 
	{
		driver.findElement(By.id("2")).click();
		driver.findElement(By.xpath("//*[@id=\"/company/globalCreditLimit/view/\"]/a")).click();
		driver.findElement(By.xpath("//*[@id=\"globalCreditLimitTable:j_idt115:filter\"]")).click();
		driver.findElement(By.id("globalCreditLimitTable:j_idt115:filter")).sendKeys("Limite Credito Test");
		driver.findElement(By.xpath("//*[@id=\"globalCreditLimitTable_data\"]/tr/td[1]/div/a[1]")).click();		
				
	}

	
}
