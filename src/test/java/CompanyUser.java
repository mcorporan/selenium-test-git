
import java.awt.List;
import java.util.concurrent.TimeUnit;
import java.security.SecureRandom;
import java.util.UUID;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CompanyUser {
	
	
	
	public static String fripickID;
	public static String nameRandom;
	public static String lastNameRandom;
	public static String code;
	public static String DNI;
	
	
	
	public static void Random(WebDriver driver) 

	{
		
		//nameRandom = UUID.randomUUID().toString();	
	  //  System.out.println("INFO "+ nameRandom);
		nameRandom = UUID.randomUUID().toString().toUpperCase().substring(0, 6);
		lastNameRandom = UUID.randomUUID().toString().toUpperCase().substring(0, 6);
		code = UUID.randomUUID().toString().toUpperCase().substring(0, 6);
		DNI = UUID.randomUUID().toString().toUpperCase().substring(0, 6);
		
		
	    
	  }
	 

	public static void createuser(WebDriver driver) {


		driver.findElement(By.id("2")).click();
		driver.findElement(By.xpath("//*[@id=\"/company/user/view/\"]/a")).click();
		driver.findElement(By.xpath("//*[@id=\"j_idt118\"]")).click();
		driver.findElement(By.id("user_name")).sendKeys(nameRandom);
		driver.findElement(By.id("user_lastName")).sendKeys(lastNameRandom);
		WebElement selectgender =	 driver.findElement(By.id("user_gender"));
		selectgender.sendKeys("Femenino");
		driver.findElement(By.id("user_address")).sendKeys("dirre");
		WebElement selectdni =	 driver.findElement(By.id("identificationType"));
		selectdni.sendKeys("Otros");
		driver.findElement(By.id("user_employeeID")).sendKeys(code);
		WebElement dni = driver.findElement(By.id("user_dni")); 
		dni.click();
		dni.sendKeys(DNI);
		driver.findElement(By.id("phone1")).click();;
		driver.findElement(By.id("phone1")).sendKeys("8097459874");// No se esta guardando con esa data
		//driver.findElement(By.id("email")).sendKeys("notengo4@gmail.com");
		WebElement typesUser =	 driver.findElement(By.id("userTypes"));
		typesUser.sendKeys("N�mina");
		WebElement groupoNomna =	 driver.findElement(By.id("discountPeriod"));
		groupoNomna.sendKeys("Aut. Creacion");
		WebElement crediLimt =	 driver.findElement(By.id("globalCreditLimit"));
		crediLimt.sendKeys("Aut. Creacion");
		WebElement subs =	 driver.findElement(By.id("subsidyGroups"));
		subs.sendKeys("Aut. Creacion");
		WebElement asigna =	 driver.findElement(By.name("j_idt140"));
		asigna.sendKeys("Aut. Creacion");
		WebElement asignadiaria =	 driver.findElement(By.name("j_idt144"));
		asignadiaria.sendKeys("Aut. Creacion");
		WebElement centrocosto = driver.findElement(By.id("costCenters"));
		centrocosto.sendKeys("Aut. Creacion");
		driver.findElement(By.id("userMovil")).click();
		
		 try {
				synchronized (driver) {
					driver.wait(2000);
				}

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		WebElement  sub  = driver.findElement(By.id("UB-01"));
		sub.sendKeys("Aut. Creacion");
		
		
		
		driver.findElement(By.id("j_idt160")).click();//Botton Save
		
		try {
			synchronized (driver) {
				driver.wait(2000);
			}

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebElement fripickIDElement = driver.findElement(By.xpath("//*[@id=\"windowUserInfoDialog\"]/div[2]/div[1]/div/div[1]/label[2]"));
		fripickID = fripickIDElement.getText();
		
		driver.findElement(By.xpath("//*[@id=\"j_idt182\"]/span[2]")).click();
		
		WebElement msg = driver.findElement(By.xpath("//*[@id=\"generalMessage\"]/div/ul/li/span"));
		String text = msg.getText();
		System.out.println("La creacion de usuarios: " + text );

	}
	
	
	public static void edituser(WebDriver driver) 
	{

		//driver.findElement(By.id("2")).click();// When i finish this metho remove this line
		//driver.findElement(By.xpath("//*[@id=\"/company/user/view/\"]/a")).click();//// When i finish this metho remove this line
		driver.findElement(By.id("searchText")).clear();
		driver.findElement(By.id("searchText")).sendKeys(fripickID);// En espera de elemento fripick id
		  driver.findElement(By.id("ajaxSearch")).click();
		  
			try {
				synchronized (driver) {
					driver.wait(2000);
				}

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  WebDriverWait wait1 = new WebDriverWait(driver, 10);
		  
		  wait1.until((ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"dataTable_data\"]/tr/td[1]/div/a[1]/i"))));
		  driver.findElement(By.xpath("//*[@id=\"dataTable_data\"]/tr/td[1]/div/a[1]/i")).click();
		  driver.findElement(By.id("user_name")).clear();;
		  driver.findElement(By.id("user_name")).sendKeys("Edit 31 name 1");
		  driver.findElement(By.id("user_lastName")).clear();;
		  driver.findElement(By.id("user_lastName")).sendKeys("Edit 31 Last 1");
		  driver.findElement(By.id("user_employeeID")).click();
		  driver.findElement(By.id("user_employeeID")).clear();
		  driver.findElement(By.id("user_employeeID")).sendKeys("Edit31cod 1");
		  driver.findElement(By.id("user_email")).click();
		  driver.findElement(By.id("user_email")).clear();
		  driver.findElement(By.id("user_email")).sendKeys("notengo96@gmail.com");
		  
		  wait1.until((ExpectedConditions.visibilityOfElementLocated(By.id("discountPeriod"))));
		  WebElement Groupnomina =	 driver.findElement(By.id("discountPeriod"));
		  Groupnomina.sendKeys("Aut. Edicion");
		
		  wait1.until((ExpectedConditions.visibilityOfElementLocated(By.id("globalCreditLimit"))));
		  WebElement selectlimi2 =	 driver.findElement(By.id("globalCreditLimit"));
		  selectlimi2.sendKeys("Aut. Edicion");
		  
		  wait1.until((ExpectedConditions.visibilityOfElementLocated(By.id("subsidyGroups"))));
		  WebElement EditSub = driver.findElement(By.id("subsidyGroups"));
		  EditSub.sendKeys("Aut. Edicion");
		  
		  wait1.until((ExpectedConditions.visibilityOfElementLocated(By.name("j_idt140"))));
		  WebElement editAsig = driver.findElement(By.name("j_idt140"));
		  editAsig.sendKeys("Aut. Edicion");
		  
		  wait1.until((ExpectedConditions.visibilityOfElementLocated(By.name("j_idt144"))));
		  WebElement editdiaria = driver.findElement(By.name("j_idt144"));
		  editdiaria.sendKeys("Aut. Edicion");
		  
		  wait1.until((ExpectedConditions.visibilityOfElementLocated(By.id("costCenters"))));
		  WebElement editCentro = driver.findElement(By.id("costCenters"));
		  editCentro.sendKeys("Aut. Edicion");
		  
		  wait1.until((ExpectedConditions.visibilityOfElementLocated(By.id("UB-01"))));
		  WebElement editBranchofice = driver.findElement(By.id("UB-01"));
		  editBranchofice.sendKeys("Aut. Edicion");
		  
		  driver.findElement(By.xpath("//*[@id=\"j_idt160\"]/span[2]")).click();//Botton Save
		 
		  
		  wait1.until((ExpectedConditions.visibilityOfElementLocated(By.id("j_idt176"))));
		  WebElement Confirm = driver.findElement(By.id("j_idt176"));
		  Confirm.click();
		  
		  WebElement msg= driver.findElement(By.xpath("//*[@id=\"generalMessage\"]/div/ul/li/span"));
		  String text = msg.getText();
		  System.out.println("Los cambio de la Edicion: " + text);
		  
	
		  

	
		
		
		
	}
	
	public static void resetPassword(WebDriver driver) 
	{
		//driver.findElement(By.id("2")).click();// When i finish this metho remove this line
		//driver.findElement(By.xpath("//*[@id=\"/company/user/view/\"]/a")).click();//// When i finish this metho remove this line
		driver.findElement(By.id("searchText")).sendKeys(fripickID);
		driver.findElement(By.id("ajaxSearch")).click();
		
		try {
			synchronized (driver) {
				driver.wait(2000);
			}

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebDriverWait wait1 = new WebDriverWait(driver, 10);
	
		wait1.until((ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"dataTable:0:buttonPassReset\"]"))));
		driver.findElement(By.xpath("//*[@id=\"dataTable:0:buttonPassReset\"]")).click();
		
		wait1.until((ExpectedConditions.visibilityOfElementLocated(By.id("confirmResetPass"))));
		driver.findElement(By.id("confirmResetPass")).click();
		
		wait1.until((ExpectedConditions.visibilityOfElementLocated(By.id("j_idt189"))));
		driver.findElement(By.id("j_idt189")).click();
		
		System.out.println("La contrase�a se cambio con exito ");
		
		
		
	}
	
	
	public static void desactivatUser (WebDriver driver) 
	{
		//driver.findElement(By.id("2")).click();
		//driver.findElement(By.xpath("//*[@id=\"/company/user/view/\"]/a")).click();
		driver.findElement(By.id("searchText")).sendKeys(fripickID);
		driver.findElement(By.id("ajaxSearch")).click();
		
		driver.findElement(By.xpath("//*[@id=\"dataTable:0:buttonUnactivate\"]")).click();
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		driver.findElement(By.id("confirmUnactivate")).click();
			

	} 
	
	
	public static void Suspend (WebDriver driver) 
	{
		
		
	//	driver.findElement(By.id("2")).click();
	   // driver.findElement(By.xpath("//*[@id=\"/company/user/view/\"]/a")).click();
	    WebDriverWait wait1 = new WebDriverWait(driver, 10);
	    
	    try {
			synchronized (driver) {
				driver.wait(2000);
			}

		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    wait1.until((ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"j_idt106_toggler\"]/span"))));
	    driver.findElement(By.xpath("//*[@id=\"j_idt106_toggler\"]/span")).click();
	   
	    
		wait1.until((ExpectedConditions.visibilityOfElementLocated(By.id("searchText"))));
		driver.findElement(By.id("searchText")).clear();
		
		  wait1.until((ExpectedConditions.visibilityOfElementLocated(By.id("searchText"))));
		  WebElement searchsu = driver.findElement(By.id("searchText"));
		  searchsu.sendKeys(fripickID);
		
		wait1.until((ExpectedConditions.visibilityOfElementLocated(By.id("ajaxSearch"))));
		driver.findElement(By.id("ajaxSearch")).click();
		
		  try {
				synchronized (driver) {
					driver.wait(2000);
				}

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  
		wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"dataTable:0:buttonSuspend\"]")));
		driver.findElement(By.xpath("//*[@id=\"dataTable:0:buttonSuspend\"]")).click();
		
		wait1.until((ExpectedConditions.visibilityOfElementLocated(By.id("confirmsuspend"))));
		driver.findElement(By.id("confirmsuspend")).click();
		
		
	WebElement msjsaved = driver.findElement(By.xpath("//*[@id=\"generalMessage3\"]/div/ul/li/span"));
	String text = msjsaved.getText();
	System.out.println("this is successfully: " +text);

		
	
		
 
	}
	
	

	

}
